﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class BackgroundManager : MonoBehaviour
    {
        private Animator anim;
        void Start()
        {
            anim = GetComponent<Animator>();
            GameManager.Instance.OnRestarted += Restart;
            ScoreManager.Instance.OnChangeScene += ChangeColor;
        }

        private void ChangeColor()
        {
            anim.SetBool("isStageChange", true);
        }

        private void Restart()
        {            
            anim.SetBool("isStageChange",false);
        }
    }
}

